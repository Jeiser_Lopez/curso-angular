import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { DestinoViaje } from './destino-viaje.model';
import { HttpClientModule } from '@angular/common/http';

// Estados
export interface DestinoViajesState{
	items: DestinoViaje[];
	loading: boolean; //Se usara despues para una barra de progreso
	favorito: DestinoViaje;
}

/*export const InitializeDestinosViajesState = function () {
	return {
		items: [],
		loading: false,
		favorito: null
	};
} tambien se puede hacer como la forma siguiente*/

export function InitializeDestinosViajesState(){
	return {
		items: [],
		loading: false,
		favorito: null
	};
}

//Acciones
export enum DestinosViajesActionTypes{
	NUEVO_DESTINO = '[Destinos Viajes] Nuevo',
	ELEGIDO_FAVORITO = '[Destinos Viajes] Favorito',
	VOTE_UP = '[Destinos Viajes] Vote Up',
	VOTE_DOWN = '[Destinos Viajes] Vote Down',
	INIT_MY_DATA = '[Destino Viajes] Init My Data'
	//Define los string de todos los estados del modulo
}

export class NuevoDestinoAction implements Action {
	type = DestinosViajesActionTypes.NUEVO_DESTINO;
	constructor(public destino: DestinoViaje) {
	}
}

export class ElegidoFavoritoAction implements Action {
	type = DestinosViajesActionTypes.ELEGIDO_FAVORITO;
	constructor(public destino: DestinoViaje) {
	}
}

export class VoteUpAction implements Action {
	type = DestinosViajesActionTypes.VOTE_UP;
	constructor(public destino: DestinoViaje) {
	}
}

export class VoteDownAction implements Action {
	type = DestinosViajesActionTypes.VOTE_DOWN;
	constructor(public destino: DestinoViaje) {
	}
}

export class InitMyDataAction implements Action {
	type = DestinosViajesActionTypes.INIT_MY_DATA;
	constructor(public destinos: string[]){}
}

export type DestinoViajesActions = NuevoDestinoAction | ElegidoFavoritoAction
| VoteDownAction | VoteUpAction | InitMyDataAction; // Agrupa los tipos de datos a usara

// Reduccer, cuando se dispara una accion son llamados en el orden que estan aqui,
export function reducerDestinosViajes(
	state: DestinoViajesState,
	action: DestinoViajesActions):
 DestinoViajesState {
	switch (action.type) {
		case DestinosViajesActionTypes.INIT_MY_DATA:
		{
			const destinos: string[] = (action as InitMyDataAction).destinos;
			return {
				...state,
				items: destinos.map((d) => new DestinoViaje(d, ''))
			};
		}
		case DestinosViajesActionTypes.NUEVO_DESTINO:
		{
			return {
				...state,
				items: [...state.items,(action as NuevoDestinoAction).destino]
			};
		}
		
		case DestinosViajesActionTypes.ELEGIDO_FAVORITO: {
			state.items.forEach(x => x.setSelected(false));
			const fav: DestinoViaje = (action as ElegidoFavoritoAction).destino;
			fav.setSelected(true);
			return {
				...state,
				favorito: fav
			};
		}

		case DestinosViajesActionTypes.VOTE_UP: {
			const d: DestinoViaje = (action as VoteUpAction).destino;
			d.VoteUp();
			return {
				...state};
		}

		case DestinosViajesActionTypes.VOTE_DOWN: {
			const d: DestinoViaje = (action as VoteDownAction).destino;
			d.VoteDown();
			return {
				...state};
		}
	}
	return state;
}

//Efectos, despues que se dispara la accion va a los reducer, ellos cambian el estadp
//Se pasa la accion a los efectos, etos realizan una nueva accion generando una acccion


@Injectable()
export class DestinoViajesEffects {
	@Effect()
	nuevoAgregado$: Observable<Action> = this.actions$.pipe(
		ofType(DestinosViajesActionTypes.NUEVO_DESTINO),
		map((action: NuevoDestinoAction) => new ElegidoFavoritoAction(action.destino))
		);
	constructor(private actions$: Actions) {
		// code...
	}
}