import { importExpr } from "@angular/compiler/src/output/output_ast";
import { DestinoViaje } from "./destino-viaje.model";
import { DestinoViajesActions, DestinoViajesState, InitializeDestinosViajesState, InitMyDataAction, NuevoDestinoAction, reducerDestinosViajes } from "./destino-viajes-state.model";

import [
    reducerDestinosViajes,
    DestinoViajesState,
    InitializeDestinosViajesState,
    InitMyDataAction,
    NuevoDestinoAction
] from './destino-viajes-state.model';

import [ DestinoViaje ] from './destino-viaje.model';

describe('reducerDestinoViajes', )= => {
    it('Should reduce init data', () => {
        //Setup
        const prevState: DestinosViajesState = InitializeDestinosViajesState();
        const action: InitMyDataAction = new InitMyDataAction(['destino 1', 'destino 2']);
        //Action
        const newState: DestinoViajesState = reducerDestinosViajes(prevState, action);
        //assertions
        expect(newState.items.length).toEqual(2);
        expect(newState.items[0].nombre).toEqual('destino 1');

        //tear down ()<Borrar lo que se inserto en una bd
    });

    it('Should reduce new item added', () => {
        const prevState: DestinoViajesState = InitializeDestinosViajesState();
        const action: NuevoDestinoAction = new NuevoDestinoAction (new DestinoViaje('Barcelona', 'url'));
        const newState: DestinoViajesState = reducerDestinosViajes(prevState, action);
        expect(newState.items.length).toEqual(2);
        expect(newState.items[0].nombre).toEqual('Barcelona');;
    });
});