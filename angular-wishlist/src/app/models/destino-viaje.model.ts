import { v4 as uuid } from 'uuid';

export class DestinoViaje{
	
	//nombre:string;
	//imagenUrl:string;

	/*constructor(n:string,u:string)
	{
		this.nombre=n;
		this.imagenUrl=u;
	}*/

	//Se pasan las variables publicas directamente al constructor y no se debe hace la asignacion dentro de este
	selected: boolean;
	servicios: string[];
	id = uuid();
		
	constructor(public nombre: string, public u: string, public votes: number = 0){
		//this.selected = false;
		this.servicios = ['Piscina','Desayuno'];
	}

	
	setSelected(s: boolean){
		this.selected = s;
	}

	isSelected(): boolean{
		return this.selected;
	}

	VoteUp() {
		this.votes++;
	}

	VoteDown() {
		this.votes--;
	}
}