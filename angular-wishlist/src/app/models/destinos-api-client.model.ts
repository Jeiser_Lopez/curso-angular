import { DestinoViaje } from './destino-viaje.model';
import { Subject, BehaviorSubject} from 'rxjs';
import { AppConfig, AppState, APP_CONFIG, db } from '../app.module';
import { Store } from '@ngrx/store';
import { ElegidoFavoritoAction, NuevoDestinoAction } from './destino-viajes-state.model';
import { forwardRef, Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';

@Injectable()
export class DestinosApiClient{

	destinos: DestinoViaje[] = [] ;
	//current: Subject<DestinoViaje> = new BehaviorSubject<DestinoViaje>(null); //propaga el evento

	constructor(private store: Store<AppState>, @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig, private http: HttpClient) {
		this.store.select(state => state.destinos)
		.subscribe((data) => {
			console.log('destinos sub store');
			console.log(data);
			this.destinos = data.items;
		});
		this.store.subscribe((data) => {
			console.log('all store');
			console.log(data);
		});
      // this.store.dispatch(new NuevoDestinoAction(d));
	}

	add(d:DestinoViaje){
	 // this.destinos.push(d);
	 //this.store.dispatch(new NuevoDestinoAction(d));

	 //Add con web service
	 const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN', 'token-seguridad'});
	 const req = new HttpRequest('POST', this.config.apiEndPoint + '/my', { nuevo: d.nombre }, { headers: headers });
	 this.http.request(req).subscribe((data: HttpResponse<{}>) => {
		 if(data.status === 200){
			 this.store.dispatch(new NuevoDestinoAction(d));
			 const myDB = db;
			 myDB.destinos.add(d);
			 console.log('Todos los destinos de la bd');
			 myDB.destinos.toArray().then(destinos => console.log(destinos));
		 }
	 });
	}

	getAll(): DestinoViaje[] {
	  return this.destinos; 
    }

    getById(id: string): DestinoViaje{
    	return this.destinos.filter(function(d) {return d.id.toString() == id; })[0];
    }

    elegir(d: DestinoViaje){
		this.store.dispatch(new ElegidoFavoritoAction(d));
    /*	this.destinos.forEach(x => x.setSelected(false));
    	d.setSelected(true);
    	this.current.next(d);//Elegido actualmente*/
    }

   /* subscribeOnChange(fn){
    	this.current.subscribe(fn);
    }*/
}