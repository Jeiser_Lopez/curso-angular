import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, Injectable, InjectionToken, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StoreModule as NgRxStoreModule, ActionReducerMap, Store} from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { HttpClient, HttpClientModule, HttpHeaders, HttpRequest } from '@angular/common/http';
import Dexie from 'dexie'
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';

import { AppComponent } from './app.component';
import { DestinoViajeComponent } from './components/destino-viaje/destino-viaje.component';
import { ListaDestinosComponent } from './components/lista-destinos/lista-destinos.component';
import { DestinoDetalleComponent } from './components/destino-detalle/destino-detalle.component';
import { FormDestinoViajeComponent } from './components/form-destino-viaje/form-destino-viaje.component';
import { 
        DestinoViajesEffects, 
        DestinoViajesState, 
        InitializeDestinosViajesState, 
        reducerDestinosViajes } from './models/destino-viajes-state.model';
import { LoginComponent } from './components/login/login/login.component';
import { ProtectedComponent } from './components/protected/protected/protected.component';
import { UsuarioLogueadoGuard } from './guards/usuario-logueado/usuario-logueado.guard';
import { environment } from '../environments/environment'; //Angular CLI environment
import { AppRoutingModule } from './app-routing.module';

import { DestinosApiClient } from './models/destinos-api-client.model';

//import { ActionReducerMap, StoreFeatureModule as NgRxStoreModule1, StoreFeatureModule } from '@ngrx/store';
import { AuthService } from './services/auth.service';
import { VuelosComponentComponent } from './components/vuelos/vuelos-component/vuelos-component.component';
import { VuelosMainComponentComponent } from './components/vuelos/vuelos-main-component/vuelos-main-component.component';
import { VuelosMasInfoComponentComponent } from './components/vuelos/vuelos-mas-info-component/vuelos-mas-info-component.component';
import { VuelosDetalleComponentComponent } from './components/vuelos/vuelos-detalle-component/vuelos-detalle-component.component';
import { ReservasModule } from './reservas/reservas.module';
import { DestinoViaje } from './models/destino-viaje.model';
import { Observable } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { NgxMapboxGLModule } from 'ngx-mapbox-gl';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EspiameDirective } from './espiame.directive';
import { TrackearClickDirective } from './trackear-click.directive';

//App config
export interface AppConfig {
  apiEndPoint: string;
}

const APP_CONFIG_VALUE: AppConfig = {
  apiEndPoint: "http://localhost/3000"
};

export const APP_CONFIG = new InjectionToken<AppConfig>('app-config');

//Fin app config

//init routing
export const childrenRoutesVuelos: Routes = [
  { path: '', redirectTo: 'main', pathMatch: 'full'},
  { path: 'main', component: VuelosMainComponentComponent},
  { path: 'mas-info', component: VuelosMasInfoComponentComponent},
  { path: 'id', component: VuelosDetalleComponentComponent}
];//rutas hijas se deben configurar en routes para que las rutas raiz sepan de ellas

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full'},
  { path: 'home', component: ListaDestinosComponent},
  { path: 'destino/:id',  component: DestinoDetalleComponent},
  { path: 'login', component: LoginComponent},
  { path: 'protected', component: ProtectedComponent, canActivate: [UsuarioLogueadoGuard]},
  { path: 'vuelos', component: VuelosComponentComponent, canActivate: [UsuarioLogueadoGuard], children: childrenRoutesVuelos}
];
//fin routing 

//init app
export function init_app(apploadService: AppLoadService): () => Promise<any> {
  return  () => apploadService.initialazeDestinoViajesState();
}
@Injectable()
class AppLoadService {
  constructor(private store: Store<AppState>, private http: HttpClient){
  }
  async initialazeDestinoViajesState(): Promise<any>{
    const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
    const req = new HttpRequest('GET', APP_CONFIG_VALUE.apiEndPoint + '/my', { headers: headers });
    const response: any = await this.http.request(req).toPromise();
    this.store.dispatch(new initMyDataAction(response.body));
  }
}
//fin app

//Redux init
export interface AppState{
  destinos: DestinoViajesState;
}
//Se define estado global de la aplicacion 

//REducer globales
const reducers: ActionReducerMap<AppState> = {
  destinos: reducerDestinosViajes
};

let reducersInitialState = {
  destinos: InitializeDestinosViajesState()
};
//redux fin

//Init dixie db
/*@Injectable({
  providedIn: 'root'
})
export class MyDataBase extends Dexie {
  destinos: Dexie.Table<DestinoViaje, number>;
  constructor (){
    super('MyDataBase');
    this.version(1).stores({
      destinos: '++id, nombre, imagenUrl',});
  }
}

export const db = new MyDataBase();*/
//Fin dixie db

//init dixie con translate
export class Translation{
  constructor(public id: number, public lang: string, public key: string, public value: string){}
}

@Injectable({
  providedIn: 'root'
})

export class MyDataBase extends Dexie {
  destinos: Dexie.Table<DestinoViaje, number>;
  translations: Dexie.Table<Translation, number>;
 
  constructor (){
    super('MyDataBase');
    this.version(1).stores({
      destinos: '++id, nombre, imagenUrl',});
    this.version(2).stores({
      destinos: '++id, nombre, imagenUrl',
      translations: '++id, lang, key, value'});
  }
}

//init i18n
class TranslationLoader implements TranslateLoader{
  constructor(private http: HttpClient){
  }
  getTranslation(lang: string): Observable<any>{
    const promise = db.translations.where('lang')
                                  .equals(lang)
                                  .toArray()
                                  .then(results => {
                                    if (results.length === 0){
                                      return this.http
                                      .get<Translation[]>(APP_CONFIG_VALUE.apiEndPoint + '/api/translation?lang=' + lang)
                                      .toPromise()
                                      .then(apiResults => {
                                        db.translations.bulkAdd(apiResults);
                                        return apiResults;
                                      });
                                    }
                                    return results;
                                  }).then((traducciones) => {
                                    console.log('Traducciones Cargadas');
                                    console.log(traducciones);
                                    return traducciones;
                                  }).then((traducciones) => {
                                    return traducciones.map((t) => ({ [t.key]: t.value}));
                                  });
                      return from(promise).pipe(flatMap((elems) => from(elems)));
  };
}
//fin i18

export const db = new MyDataBase();

function HttpLoaderFactory(http: HttpClient){
  return new TranslationLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    DestinoViajeComponent,
    ListaDestinosComponent,
    DestinoDetalleComponent,
    FormDestinoViajeComponent,
    NgRxStoreModule,
    LoginComponent,
    ProtectedComponent,
    VuelosComponentComponent,
    VuelosMainComponentComponent,
    VuelosMasInfoComponentComponent,
    VuelosDetalleComponentComponent,
    EspiameDirective,
    TrackearClickDirective
  ],
  
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    NgRxStoreModule.forRoot(reducers, { initialState: reducersInitialState }),
    EffectsModule.forRoot([DestinoViajesEffects]),
    StoreDevtoolsModule.instrument(),
    ReservasModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (HttpLoaderFactory),
        deps: [HttpClient]
      }
    }),
    NgxMapboxGLModule,
    BrowserAnimationsModule,
  ],
//  providers: [DestinosApiClient, AuthService, UsuarioLogueadoGuard],
  providers: [AuthService, UsuarioLogueadoGuard,
    {provide: APP_CONFIG, useValue: APP_CONFIG_VALUE },
    AppLoadService,
      { provide: APP_INITIALIZER, useFactory: init_app, deps: [AppLoadService], multi: true} ],
      MyDataBase,
  bootstrap: [AppComponent]     
})

export class AppModule { }
