import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { ElegidoFavoritoAction, NuevoDestinoAction } from '../../models/destino-viajes-state.model';
import { DestinoViaje } from './../../models/destino-viaje.model';
import { DestinosApiClient } from './../../models/destinos-api-client.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers: [DestinosApiClient] //Se agrega la inyeccion
})
export class ListaDestinosComponent implements OnInit {

  //destinos: DestinoViaje[];
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  updates: String[];
  //all;//Se le asignaran los cambios de estado

 /* constructor() { 
  	this.destinos=[];
  }*/

  constructor(private destinosApiClient: DestinosApiClient, private store: Store<AppState>){

    this.onItemAdded = new EventEmitter();
    this.updates = [];
   /* this.onItemAdded = new EventEmitter();
    this.updates = [];
    this.store.select(state => state.destinos.favorito)
      .subscribe(d => {
          if (d != null){
            this.updates.push('Se ha elegido a ' + d.nombre);
          }
       });
       store.select(state => state.destinos.items).subscribe(items => this.all = items);
    //this.destinosApiClient.subscribeOnChange((d: DestinoViaje) => {
      //if (d != null){
       // this.updates.push('Se ha elegido a ' + d.nombre);
     // }
   // });*/
  }

  ngOnInit(): void {
    this.store.select(state => state.destinos.favorito)
      .subscribe(data => {
        const f = data;
        if (f != null){
          this.updates.push('Se eligio: '+ f.nombre);
        }
      });
  }

  /*guardar(nombre:string,url:string):boolean{
    this.destinos.push(new DestinoViaje(nombre,url));
  	return false;

  }*/

  agregado(d: DestinoViaje){
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
    //this.store.dispatch(new NuevoDestinoAction(d));
  }

  /*elegido(destino :DestinoViaje){
    this.destinos.forEach(function(x){
      x.setSelected(false);
    });
    destino.setSelected(true);
  }*/
// Se removieron las variables internas del api client, para usar todo mediante redux
//por eso ya no se usa  this.store.dispatch(new ElegidoFavoritoAction(e));


  elegido(e: DestinoViaje){
    this.destinosApiClient.elegir(e);
   // this.store.dispatch(new ElegidoFavoritoAction(e));
  //  this.destinosApiClient.getAll().forEach(x => x.setSelected(false));
    //e.setSelected(true); Se paasa la responsabilidad al destinos-api-client
  }

  getAll(){
    
  }

}
