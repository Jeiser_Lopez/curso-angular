import { Component, OnInit, Input, HostBinding,EventEmitter,Output } from '@angular/core';
import { AppState } from '../../app.module';
import { VoteDownAction, VoteUpAction } from '../../models/destino-viajes-state.model';
import { Store } from '@ngrx/store';
import { DestinoViaje } from './../../models/destino-viaje.model';

import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css'],
  animations: [
    trigger('esFavorito', [
      state('estadoFavorito', style ({
        backgroundColor: 'PaleTurquoise'
      })),
      state('estadoNoFavorito', style({
        backgroundColor: 'whiteSmoke'
      })),
      transition('estadoNoFavorito => estadoFavorito', [
        animate('3s')
      ]),
      transition('estadoFavorito => estadoNoFavorito', [
        animate('1s')
      ]),
    ])
  ]
})

export class DestinoViajeComponent implements OnInit {
  @Input() destino: DestinoViaje;
  @HostBinding('attr.class') cssClass='col-md-4';
  @Output() clicked: EventEmitter<DestinoViaje>;
  @Input('idx') posicion: number; //Nombre personalizado

  constructor(private store: Store<AppState>) { 
  	this.clicked = new EventEmitter();
  	//this.destino=new DestinoViaje();
  }

  ngOnInit(): void {
  }

  ir(){
  	this.clicked.emit(this.destino);
  	return false;//no recargar la pagina
  }

  voteUp(){
    this.store.dispatch(new VoteUpAction(this.destino));
    return false;
  }

  voteDown(){
    this.store.dispatch(new VoteDownAction(this.destino));
    return false;
  }

}
